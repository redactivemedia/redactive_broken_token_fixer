# Redactive Broken Token Fixer

This module fixes the case where an incomplete module_hook_info() call
occurs.

This results in an incomplete list of module hook info being cached.

This seems to occur when using Drupal's "Clear all caches" button on the
admin > configuration > development page, or the "Clear all caches" link
in the Drupal Admin Menu module.

Symptoms of this issue occurring include Node tokens not working, and the
presence of errors on pages which use Tokens such as the Pathauto
configuration page, such as:

`Notice: Undefined index: name in token_asort_tokens() (line 582 of ...
  token.module).`

`Default path pattern (applies to all content types with blank patterns
  below) is using the following invalid tokens: [node:title].`


Because some data exists in the cache, Drupal assumes the data can be
trusted when it cannot.

This issue is a particularly insidious one because it breaks almost all
tokens, and as a result breaks a number of sites' layouts when they use
tokens to set a path off which block placements hang - e.g. /news/*.

This module invalidates caches when they don't contain a mention of
the Node module's token_info hook, since this should always be present
if the node module is enabled.

Questions/comments/complaints to digital@redactive.co.uk please (-:

Alex Harries
6th November 2019